package isi.Coupe_du_Monde.Controller;

import java.util.List;

import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import isi.Coupe_du_Monde.Entities.*;
import isi.Coupe_du_Monde.Repository.*;


@RestController
@RequestMapping("/api")
public class ArbController {
	@Autowired 
	arbRepo po;
	
	
	@PostMapping("/addArb")
	public Arbitre createArb(@Valid @RequestBody Arbitre arb) {
	    return po.save(arb);
	}
	
	@GetMapping("/arbitres")
	public List<Arbitre> getAllArb() {
		List<Arbitre> pro = po.findAll();

        return pro;
	}
}
