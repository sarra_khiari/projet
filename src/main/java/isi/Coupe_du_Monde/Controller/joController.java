package isi.Coupe_du_Monde.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import isi.Coupe_du_Monde.Entities.*;
import isi.Coupe_du_Monde.Repository.*;

@RestController
@RequestMapping("/api")
public class joController {
	@Autowired 
	joRepo po;
	
	
	@PostMapping("/addJo")
	public Joueur createJO(@Valid @RequestBody Joueur jo) {
	    return po.save(jo);
	}
	
	@GetMapping("/joueurs")
	public List<Joueur> getAllJo() {
		List<Joueur> pro = po.findAll();

        return pro;
	}
}
