package isi.Coupe_du_Monde.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import isi.Coupe_du_Monde.Entities.*;
import isi.Coupe_du_Monde.Repository.*;


@RestController
@RequestMapping("/api")
public class specController {
	@Autowired 
	specRepo po;

	
	@Autowired 
	jeuRepo cpo;
	
	
	@PostMapping("/addspec")
	public Spectateur createSpec(@Valid @RequestBody Spectateur spec) {
	    return po.save(spec);
	}
	
	@GetMapping("/spectateurs")
	public List<Spectateur> getAllspec() {
		List<Spectateur> pro = po.findAll();

        return pro;
        
	}
	
	
	@PutMapping("/affecterspec/{aid}/{pid}")
	public void affecterspec(@PathVariable( "aid") Long Id,
			@PathVariable("pid") Long Idp) {

	    
	   
		Spectateur spec = po.findById(Id).get();
		  Jeu je=cpo.findById(Idp).get();
		   
		 
		  spec.setJeux(je);
		   
		
		   po.save(spec);
		
	

	}
}
