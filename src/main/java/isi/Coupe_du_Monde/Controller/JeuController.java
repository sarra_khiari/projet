package isi.Coupe_du_Monde.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import isi.Coupe_du_Monde.Entities.*;
import isi.Coupe_du_Monde.Repository.*;



@RestController
@RequestMapping("/api")
public class JeuController {
	@Autowired 
	jeuRepo po;
	
	@Autowired 
	joRepo apo;
	
	@Autowired 
	arbRepo cpo;
	
	
	@PostMapping("/addjeu")
	public Jeu createJeu(@Valid @RequestBody Jeu jeu) {
	    return po.save(jeu);
	}
	
	@GetMapping("/matchs")
	public List<Jeu> getAllJeu() {
		List<Jeu> pro = po.findAll();

        return pro;
	}
	
	@PutMapping("/affecterjo/{uid}/{pid}")
	public void affecterjo(@PathVariable("uid") Long Id,
			@PathVariable("pid") Long Idp) {

	    
	   
		   Jeu jeu = po.findById(Id).get();
		   Joueur jo=apo.findById(Idp).get();
		  
		   
		   jeu.setJoueurs(jo);
		 
		
		   po.save(jeu);
		
	

	}
	
	@PutMapping("/affectercarb/{aid}/{pid}")
	public void affecterarb(@PathVariable( "aid") Long Id,
			@PathVariable("pid") Long Idp) {

	    
	   
		   Jeu jeu = po.findById(Id).get();
		  Arbitre arb=cpo.findById(Idp).get();
		   
		 
		  jeu.setArbitres(arb);
		   
		
		   po.save(jeu);
		
	

	}
	
	
}