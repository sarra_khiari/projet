package isi.Coupe_du_Monde;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoupeDuMondeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoupeDuMondeApplication.class, args);
	}

}
