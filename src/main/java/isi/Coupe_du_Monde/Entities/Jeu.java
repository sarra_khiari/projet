package isi.Coupe_du_Monde.Entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
public class Jeu  implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column(name="jeuid")
	private Long id;
	private String lieu;
	private float duree;
	private double espace;
	
	@ManyToOne 

	Arbitre arbitres;
	
	@ManyToOne 

	Joueur joueurs;
	
	
	@Override
	public String toString() {
		return "jeu [id=" + id + ", lieu=" + lieu + ", duree=" + duree + ", espace=" + espace + ", arbitres=" + arbitres
				+ ", joueurs=" + joueurs + "]";
	}
	public Jeu(Long id, String lieu, float duree, double espace, Arbitre arbitres, Joueur joueurs) {
		super();
		this.id = id;
		this.lieu = lieu;
		this.duree = duree;
		this.espace = espace;
		this.arbitres = arbitres;
		this.joueurs = joueurs;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLieu() {
		return lieu;
	}
	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
	public float getDuree() {
		return duree;
	}
	public void setDuree(float duree) {
		this.duree = duree;
	}
	public double getEspace() {
		return espace;
	}
	public void setEspace(double espace) {
		this.espace = espace;
	}
	
	public Arbitre getArbitres() {
		return arbitres;
	}
	public void setArbitres(Arbitre arbitres) {
		this.arbitres = arbitres;
	}
	public Joueur getJoueurs() {
		return joueurs;
	}
	public void setJoueurs(Joueur joueurs) {
		this.joueurs = joueurs;
	}
	
	

}
