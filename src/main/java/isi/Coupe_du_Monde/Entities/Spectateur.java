package isi.Coupe_du_Monde.Entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Spectateur implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column(name="spectateurid")
	private Long id;
	private String nom;
	private int cin;
	private int num_passaport;
	
	
	@ManyToOne 

	Jeu jeux;


	public Spectateur(Long id, String nom, int cin, int num_passaport, Jeu jeux) {
		super();
		this.id = id;
		this.nom = nom;
		this.cin = cin;
		this.num_passaport = num_passaport;
		this.jeux = jeux;
	}


	@Override
	public String toString() {
		return "Spectateur [id=" + id + ", nom=" + nom + ", cin=" + cin + ", num_passaport=" + num_passaport + ", jeux="
				+ jeux + "]";
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public int getCin() {
		return cin;
	}


	public void setCin(int cin) {
		this.cin = cin;
	}


	public int getNum_passaport() {
		return num_passaport;
	}


	public void setNum_passaport(int num_passaport) {
		this.num_passaport = num_passaport;
	}


	public Jeu getJeux() {
		return jeux;
	}


	public void setJeux(Jeu jeux) {
		this.jeux = jeux;
	}
	
	
	
}
