package isi.Coupe_du_Monde.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import isi.Coupe_du_Monde.Entities.*;


@Repository
public interface specRepo extends JpaRepository<Spectateur,Long> {

}
