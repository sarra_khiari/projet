package isi.Coupe_du_Monde.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import isi.Coupe_du_Monde.Entities.*;
import isi.Coupe_du_Monde.Repository.*;
@Repository
public interface jeuRepo  extends JpaRepository<Jeu,Long>{

}
